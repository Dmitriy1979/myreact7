type ProductType = {
  name: string;
  price: number;
  weight: number;
  sum?: number;
  check?: boolean;
};

export type { ProductType };
